package ictgradschool.industry.lab07.ex06;

import ictgradschool.Keyboard;

/**
 * TODO Write a program according to the Exercise Six guidelines in your lab handout.
 */
public class ExerciseSix {

    public void start() {
        // TODO Write the codes :)

        System.out.println("Enter a string of at most 100 characters: ");

        try {
            checkValidString(getString());
        } catch (ExceedMaxStringLengthException e){
            System.out.println("The String is far too long! type under 100 characters");
        } catch (InvalidWordException e) {
            System.out.println("your characters are not all words, all words need to start with a alphabetical character!");
        }

    }

    // TODO Write some methods to help you.

    public String getString() {
        String userString = Keyboard.readInput();
        return userString;
    }

    public void checkValidString(String userString) throws ExceedMaxStringLengthException, InvalidWordException {
        if (userString.length() > 100) {
            throw new ExceedMaxStringLengthException();
        } else {
            String initialsString = "" + userString.charAt(0);
            for (int i = 0; i < userString.length(); i++) {
                if (userString.charAt(i) == (' ') && (i+1) != (' ') && (i+1 < userString.length())) {
                    if (Character.isDigit(userString.charAt(i+1))) {
                        throw new InvalidWordException();
                    }
                    initialsString = initialsString + " " + userString.charAt(i + 1);
                }
            }
            System.out.println("You entered: " + initialsString);
        }
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        new ExerciseSix().start();
    }
}
