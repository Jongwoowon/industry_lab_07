package ictgradschool.industry.lab07.ex05;

/**
 * Created by J Won on 27/03/2017.
 */
public class IndexTooLowException extends Exception {
    public IndexTooLowException(String message) {
        super(message);
    }
}
