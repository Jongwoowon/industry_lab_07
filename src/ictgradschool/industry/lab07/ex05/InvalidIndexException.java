package ictgradschool.industry.lab07.ex05;

/**
 * Created by J Won on 27/03/2017.
 */
public class InvalidIndexException extends Exception {
    public InvalidIndexException (String message) {
        super(message);
    }
}
