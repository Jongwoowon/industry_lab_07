package ictgradschool.industry.lab07.ex05;

/**
 * Created by J Won on 27/03/2017.
 */
public class IndexTooHighException extends Exception {
    public IndexTooHighException (String message){
        super(message);
    }
}
